import streamlit as st
import machine_learning as ml
import feature_extraction as fe
from bs4 import BeautifulSoup
import requests as re
import matplotlib.pyplot as plt

st.title('Phát hiện Trang web lừa đảo bằng Machine Learning')
st.write('Ứng dụng dựa trên ML này được phát triển cho mục đích giáo dục. Mục tiêu của ứng dụng là phát hiện các trang web lừa đảo chỉ bằng dữ liệu nội dung. Không phải URL!'
         ' Bạn có thể xem chi tiết về phương pháp, tập dữ liệu và tập đặc trưng nếu bạn nhấp vào _"Xem Chi Tiết"_. ')


with st.expander("CHI TIẾT DỰ ÁN"):
    st.subheader('Phương pháp')
    st.write('Tôi sử dụng _học có giám sát_ để phân loại các trang web lừa đảo và hợp pháp. '
             'Tôi tận dụng phương pháp dựa trên nội dung và tập trung vào html của các trang web. '
             'Ngoài ra, tôi sử dụng scikit-learn cho các mô hình ML.'
             )
    st.write('Đối với dự án giáo dục này, '
             'Tôi đã tạo ra tập dữ liệu của riêng mình và xác định các đặc trưng, một số từ tài liệu và một số dựa trên phân tích thủ công. '
             'Tôi sử dụng thư viện requests để thu thập dữ liệu, BeautifulSoup module để phân tích cú pháp và trích xuất các đặc trưng. ')
    st.write('Mã nguồn và tập dữ liệu có sẵn trong liên kết Github dưới đây:')
    st.write('https://gitlab.com/nguyentri908908/dectection-phishing')

    st.subheader('Tập dữ liệu')
    st.write('Tôi sử dụng _"phishtank.org"_ & _"tranco-list.eu"_ làm nguồn dữ liệu.')
    st.write('Tổng cộng 26584 trang web ==> **_16060_ trang web hợp pháp** | **_10524_ trang web lừa đảo**')
    # ----- ĐỐI VỚI BIỂU ĐỒ TRÒN ----- #
    labels = 'lừa đảo', 'hợp pháp'
    phishing_rate = int(ml.phishing_df.shape[0] / (ml.phishing_df.shape[0] + ml.legitimate_df.shape[0]) * 100)
    legitimate_rate = 100 - phishing_rate
    sizes = [phishing_rate, legitimate_rate]
    explode = (0.1, 0)
    fig, ax = plt.subplots()
    ax.pie(sizes, explode=explode, labels=labels, shadow=True, startangle=90, autopct='%1.1f%%')
    ax.axis('equal')
    st.pyplot(fig)
    # ----- !!!!! ----- #

    st.write('Đặc trưng + URL + Nhãn ==> Dataframe')
    st.markdown('Nhãn là 1 cho trang web lừa đảo, 0 cho hợp pháp')
    number = st.slider("Chọn số hàng để hiển thị", 0, 100)
    st.dataframe(ml.legitimate_df.head(number))


    @st.cache
    def convert_df(df):
        # QUAN TRỌNG: Cache việc chuyển đổi để ngăn việc tính toán mỗi lần chạy lại
        return df.to_csv().encode('utf-8')

    csv = convert_df(ml.df)

    st.download_button(
        label="Tải dữ liệu dưới dạng CSV",
        data=csv,
        file_name='phishing_legitimate_structured_data.csv',
        mime='text/csv',
    )

    st.subheader('Tính năng')
    st.write('Tôi chỉ sử dụng các đặc trưng dựa trên nội dung. Tôi không sử dụng các đặc trưng dựa trên url như độ dài của url vv.'
             'Hầu hết các đặc trưng được trích xuất bằng phương thức find_all() của module BeautifulSoup sau khi phân tích html.')

    st.subheader('Kết quả')
    st.write('Tôi sử dụng 7 phân loại ML khác nhau của scikit-learn và kiểm tra chúng bằng cách triển khai kiểm định chéo k-fold.'
             'Đầu tiên, tôi đã thu được các ma trận nhầm lẫn của chúng, sau đó tính toán điểm chính xác, độ chính xác và độ phủ sóng của chúng.'
             'Bảng so sánh ở dưới:')
    st.table(ml.df_results)
    st.write('NB --> Gaussian Naive Bayes')
    st.write('SVM --> Support Vector Machine')
    st.write('DT --> Decision Tree')
    st.write('RF --> Random Forest')
    st.write('AB --> AdaBoost')
    st.write('NN --> Neural Network')
    st.write('KN --> K-Neighbours')

with st.expander('VÍ DỤ VỀ URL LỪA ĐẢO:'):
    st.write('_https://rtyu38.godaddysites.com/_')
    st.write('_https://karafuru.invite-mint.com/_')
    st.write('_https://defi-ned.top/h5/#/_')
    st.caption('NHỚ RẰNG, CÁC TRANG WEB LỪA ĐẢO CÓ VÒNG ĐỜI NGẮN! VÌ VẬY, CÁC VÍ DỤ CẦN ĐƯỢC CẬP NHẬT!')

choice = st.selectbox("Vui lòng chọn mô hình học máy của bạn",
                 [
                     'Gaussian Naive Bayes', 'Support Vector Machine', 'Decision Tree', 'Random Forest',
                     'AdaBoost', 'Neural Network', 'K-Neighbours'
                 ]
                )

model = ml.nb_model

if choice == 'Gaussian Naive Bayes':
    model = ml.nb_model
    st.write('Mô hình GNB đã được chọn!')
elif choice == 'Support Vector Machine':
    model = ml.svm_model
    st.write('Mô hình SVM đã được chọn!')
elif choice == 'Decision Tree':
    model = ml.dt_model
    st.write('Mô hình DT đã được chọn!')
elif choice == 'Random Forest':
    model = ml.rf_model
    st.write('Mô hình RF đã được chọn!')
elif choice == 'AdaBoost':
    model = ml.ab_model
    st.write('Mô hình AB đã được chọn!')
elif choice == 'Neural Network':
    model = ml.nn_model
    st.write('Mô hình NN đã được chọn!')
else:
    model = ml.kn_model
    st.write('Mô hình KN đã được chọn!')


url = st.text_input('Nhập URL')
# kiểm tra URL có hợp lệ không
if st.button('Kiểm tra!'):
    try:
        response = re.get(url, verify=False, timeout=4)
        if response.status_code != 200:
            print(". Kết nối HTTP không thành công cho URL: ", url)
        else:
            soup = BeautifulSoup(response.content, "html.parser")
            vector = [fe.create_vector(soup)]  # nó phải là mảng 2D, vì vậy tôi đã thêm []
            result = model.predict(vector)
            if result[0] == 0:
                st.success("Trang web này dường như là hợp pháp!")
                st.balloons()
            else:
                st.warning("Chú Ý! Trang web này có thể LỪA ĐẢO!")
                st.snow()

    except re.exceptions.RequestException as e:
        print("--> ", e)
